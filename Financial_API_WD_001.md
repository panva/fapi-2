# Financial-grade API Security Profile 1.0 - Part 1: Baseline

This location previously contained the pre-final/draft of FAPI-R / FAPI1-Baseline.

The final approved version of that document can be found here:

https://openid.net/specs/openid-financial-api-part-1-1_0-final.html

The latest working version of the document (which may contain errata not yet officially published) can be found here:

https://bitbucket.org/openid/fapi/src/master/FAPI_1.0/openid-financial-api-part-1-1_0.md
